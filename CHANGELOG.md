- [Bubble UPnP Server](#bubble-upnp-server)
- [Denon Remote Control](#denon-remote-control)

# Bubble UPnP Server

### 0.9-u49-r3
* Update Alpine 3.21.0

### 0.9-u49-r2
* Update Alpine 3.20.3

### 0.9-u49-r1
* Update to 0.9 u49
* Update Alpine 3.19.1
### 0.9-u48-r1
* Update to 0.9 u48
* Update Alpine 3.19.0
* Update openjdk21-jre
### 0.9-u46-r4
* Update Alpine 3.18.4
* Removed REST service, only bubble upnp server is installed in the image
* Update to openjdk17-jre
### 0.9-u46-r3
* Update Alpine 3.18.0
### 0.9-u46-r2
* Update Alpine 3.17.3
### 0.9-u46-r1
* Update to 0.9 u46
* Updated launcher
* Update Alpine to 3.17.2
### 0.9-u45-r1
* Update to 0.9 u45
* Update Alpine to 3.17.1
### 0.9-u44-r1
* Update to 0.9 u44
* Update Alpine to 3.17.0
### 0.9-u43-r1
* Update to 0.9 u43
* Update Alpine to 3.16.1
* JRE 11
### 0.9-u42-r1
* Update to 0.9 u42
* Update Alpine to 3.15
### 0.9-u41-r2
* Fix: Update Alpine 3.14.3
### 0.9-u41-r1
* Update Bubble UPnP Server 0.9 update 41
* Update Bubble UPnP Launcher
* Update Alpine 3.14.3
### 0.9-u40-r1
* Update Bupple UPnP Server 0.9 update 40
* Update Bubble UPnP Launcher
* Update Alpine 3.13.3
### 0.9-u39
Skipped due to instability
### 0.9-u38-r1
* Update Alpine 3.13.2
### 0.9-u38-r1
* Update Bubble UPnP Server 0.9 update 38
* Update to Alpine 3.12.0
### 0.9-u37-r2
* Update to Alpine 3.11.3, use multistage image
### 0.9-u37-r1
* Update to Bubble UPnP Server 0.9 update 37 and Alpine 3.10.3
### 0.9-u33-r1
* Update to Bubble UPnP Server 0.9 update 33 and Alpine 3.10.1
### 0.9-u31-r1
* Update to Bubble UPnP Server 0.9 update 31
### 0.9-u30-r4
* Fixed missing startup parameters
### 0.9-u30-r3
* Improved tear down of the Bubble UPnP Service
### 0.9-u30-r2
* Add a rest api to start/stop the Bubble UPnP Server
### 0.9-u30-r1
* Basic Bubble UPnP Server

# Denon Remote control

### 0.9-r14
* Update Alpine 3.21.0

### 0.9-r13
* Updat Alpine 3.20.3

### 0.9-r12
* Fix python script
### 0.9-r11
Do not use, error at startup 

* Update Alpine 3.19.0
* Extend 'next' command
### 0.9-r10
* Update Alpine 3.19.0
### 0.9-r9
* Update Alpine 3.18.4
### 0.9-r8
* Update Alpine 3.18.0
### 0.9-r7
* Update Alpine 3.17.3
### 0.9-r6
* Update Alpine 3.17.2
### 0.9-r5
* Update Alpine 3.17.0
### 0.9-r4
* Update Alpine 3.15.0
### 0.9-r3
* Update Alpine 3.13.2
### 0.9-r2
* Disable Flask and werkzeug logging
### 0.9-r1
* Add zone 2 support (source, volume, power, mute)
* Update to alpine 3.11.6
### 0.8-r6
* Add source 'AUX2'
### 0.8-r5
* Update to Alpine 3.11.3
### 0.8-r4
* Introduce 'surround' requests
### 0.8-r3
* Introduce 'playing' and 'source' requests (changed 'line' request)
### 0.8-r2
* Improve connection handling
### 0.8-r1
* Throttle NSE messages (2 seconds)
### 0.7-r1
* Improved REST API functionality
  - Additional commands (volume, power, display)
  - Improved REST response values
### 0.6-r1
* Proper setup and allow configuration with environment variables
### 0.5-b3
* Obsolete
