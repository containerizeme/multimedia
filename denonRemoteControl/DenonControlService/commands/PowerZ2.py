import logging
import DataCache as cache
import commands._SimpleCommand as cmdSimple

logger = logging.getLogger(__name__)

_id = 'powerZ2'
_prefix = 'Z2'
_requests = ['ON', 'OFF', 'STANDBY']
cachedValue = cache.CachedValue(_id)


def getId():
    return _id


def cmdPrefix():
    return _prefix


def createRequest(request='get'):
    cachedValue.invalidate()
    request = request.upper()

    if request in ('GET'):
        return cmdPrefix() + '?\r'
    elif request in ('ON'):
        return cmdPrefix() + 'ON\r'
    elif request in ('STANDBY', 'OFF'):
        return cmdPrefix() + 'OFF\r'

    logger.debug('Ignore unknwon request')
    return None


def isProcessible(reply):
    if cmdSimple.isProcessible(cmdPrefix(), reply) is True:
        if reply[2:] in (_requests):
            return True
    return False


def processReply(reply):
    if isProcessible(reply) is True:
        return cmdSimple.processReply(cmdPrefix(), reply, cachedValue)
    return None
