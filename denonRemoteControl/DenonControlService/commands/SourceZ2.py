import logging
import DataCache as cache
import commands._SimpleCommand as cmdSimple

logger = logging.getLogger(__name__)

_id = 'sourceZ2'
_prefix = 'Z2'
_requests = ['TUNER', 'DVD', 'BD', 'TV', 'SAT/CBL', 'GAME', 'AUX1', 'AUX2', 'FLICKR',
             'MPLAY', 'NET', 'PANDORA', 'SIRIUSXM', 'SPOTIFY', 'FAVORITES', 'IRADIO', 'SERVER', 'USB/IPOD',
             'USB', 'IPD', 'IRP', 'FVP'
             ]
cachedValue = cache.CachedValue(_id)


def getId():
    return _id


def cmdPrefix():
    return _prefix


def createRequest(request='get'):
    cachedValue.invalidate()
    request = request.upper()

    if request in ('GET'):
        return cmdPrefix() + '?\r'
    elif request in (_requests):
        return cmdPrefix() + request + '\r'
    elif request in ('SATCBL'):
        return cmdPrefix() + 'SAT/CBL' + '\r'
    elif request in ('USBIPOD'):
        return cmdPrefix() + 'USB/IPOD' + '\r'

    logger.debug('Ignore unknwon request')
    return None


def isProcessible(reply):
    if cmdSimple.isProcessible(cmdPrefix(), reply) is True:
        if reply[2:] in (_requests):
            return True
    return False


def processReply(reply):
    if isProcessible(reply) is True:
        return cmdSimple.processReply(cmdPrefix(), reply, cachedValue)
    return None
