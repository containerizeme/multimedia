[![Open Issues](https://img.shields.io/badge/dynamic/json?color=yellow&logo=gitlab&label=open%20issues&query=%24.statistics.counts.opened&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F16773971%2Fissues_statistics)](https://gitlab.com/containerizeme/multimedia/-/issues)
[![Last Commit](https://img.shields.io/badge/dynamic/json?color=green&logo=gitlab&label=last%20commit&query=%24[:1].committed_date&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F16773971%2Frepository%2Fcommits%3Fbranch%3Dmaster)](https://gitlab.com/containerizeme/grav/-/commits/master)

[![License](https://img.shields.io/badge/dynamic/json?color=orange&label=license&query=%24.license.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F16773971%3Flicense%3Dtrue)](https://gitlab.com/containerizeme/multimedia/-/blob/master/LICENSE)
applies to software of this project. The running software within the image/container ships with its own license(s).

[![BubbleUPnPServer](https://badgen.net/badge/project/BubbleUPnPServer/orange?icon=gitlab)](https://gitlab.com/containerizeme/multimedia/-/blob/master/README.md#bubble-upnp-server)
[![Stable Version](https://img.shields.io/docker/v/icebear8/bubbleupnpserver/stable?color=informational&label=stable&logo=docker)](https://gitlab.com/containerizeme/multimedia/-/blob/master/CHANGELOG.md#bubble-upnp-server)
[![Docker Pulls](https://badgen.net/docker/pulls/icebear8/bubbleupnpserver?icon=docker&label=pulls)](https://hub.docker.com/r/icebear8/bubbleupnpserver)
[![Docker Image Size](https://badgen.net/docker/size/icebear8/bubbleupnpserver/stable?icon=docker&label=size)](https://hub.docker.com/r/icebear8/bubbleupnpserver)

[![DenonRemoteControl](https://badgen.net/badge/project/DenonRemoteControl/orange?icon=gitlab)](https://gitlab.com/containerizeme/multimedia/-/blob/master/README.md#denon-remote-control)
[![Stable Version](https://img.shields.io/docker/v/icebear8/denonservice/stable?color=informational&label=stable&logo=docker)](https://gitlab.com/containerizeme/multimedia/-/blob/master/CHANGELOG.md#denon-remote-control)
[![Docker Pulls](https://badgen.net/docker/pulls/icebear8/denonservice?icon=docker&label=pulls)](https://hub.docker.com/r/icebear8/denonservice)
[![Docker Image Size](https://badgen.net/docker/size/icebear8/denonservice/stable?icon=docker&label=size)](https://hub.docker.com/r/icebear8/denonservice)

# Bubble UPnP Server
The Bubble UPnP server allows to play media from a media library on a renderer within the network.
The Bubble UPnP App can be used as a control point.
See: [Bubble UPnP Server](http://www.bubblesoftapps.com/bubbleupnpserver/)

The images are setup to run a specific Bubble UPnP Server version.
The auto update feature is disabled by default.

## Usage
The service has to share the network interface with the host for proper usage.
Otherwise Bubble UPnP Server will not detect the media libraries and renderers within the network without additional network configuration.

`docker run -p 58050:58050 --net=host icebear8/bubbleupnpserver`

## Configuration
By default the server is started without logging and auto update is disabled (server start arguments: `-nologfile -logLevel SEVERE -disableAutoUpdate`). The arguments for the server can be overwritten with the environment variable `SERVICE_ARGS`.

`docker run -p 58050:58050 --net=host -e SERVICE_ARGS='-logLevel INFO' icebear8/bubbleupnpserver`

# Denon Remote Control
Denon service is a basic Python script to access the TCP interface of Denon receivers.
It provides a REST based interface to interact with the Denon receivers.

## Usage
`docker run -p 5000:5000 icebear8/denonservice:stable`

## Configuration
The Denon service settings are configurable with environment variables.

| Argument    | Default     | Description |
|-            |-            |-            |
| DENON_HOST  | 192.168.0.0 | IP, hostname or URL of the Denon receiver to connect (e.g. 192.168.0.42 or mydenon.local) |
| LOG_LEVEL   | ERROR       | [DEBUG, INFO, WARNING, ERROR, CRITICAL] |
| CON_TIMEOUT | 300         | Idle timeout in seconds to close the Denon TCP connection |

`docker run -p 80:5000 -e "DENON_HOST=192.168.0.42" icebear8/denonservice:stable`

## Rest API
The rest service is running on port 5000.
In case REST request occurs, 'Denon service' tries to connect to the receiver (TCP connection).
If the service is idle for a specific time (default 300 Seconds) it disconnects the TCP connection.

### Rest API Supports
* `GET /power`: Gets the current power level
* `PUT /power/<cmd>`: Sets the power. Accepted values:
  - `on`, `standby` (same as `off`)
* `GET /volume`: Gets the current volume level
* `PUT /volume/<cmd>`: Sets the volume. Accepted values:
  - `up`, `down`, <volume> as decimal
* `GET /source`: Gets the current selected source
* `PUT /source/<id>`: Sets the source. Accepted values:
  - `TUNER`, `DVD`, `BD`, `TV`, `SATCBL`, `MPLAY`, `GAME`, `AUX1`, `AUX2`, `NET`, `SPOTIFY`, `FLICKR`, `FAVORITES`, `IRADIO`, `SERVER`, `USBIPOD`
  - North America only: `PANDORA`, `SIRIUSXM`
  - Select and start playback: `USB` (USB), `IPD` (iPod),`IRP` (internet radio), `FVP` (favorites)
* `GET /surround`: Gets the current surround mode
* `PUT /surround/<mode>`: Sets the surround mode, depends on the current listening mode. Accepted values:
  - `MOVIE`, `MUSIC`, `GAME`, `DIRECT`, `STEREO`, `STANDARD`, `DOLBY_DIGITAL`, `DTS SUROUND`
  - `MCH STEREO`, `ROCK ARENA`, `JAZZ CLUB`, `MONO MOVIE`, `MATRIX`, `VIDEO`, `VIRTUAL`
  - North America only: `PURE_DIRECT`
* `GET /playing/<id>`: Gets the information about whats currently playing. Accepted values:
  - `album`, `artist`, `title`
* `GET /display/lines`: Gets the lines 0..8 from the display
* `GET /display/line/<index>`: Gets a specific display line. Accepted values:
  - <line> as decimal (0..8)
* `PUT /start`: Starts the receiver and plays the first favorite item
* `PUT /startVolume/<volume>`: Starts the receiver with volume level `<volume>` and plays the first favorite item
* `PUT /next`: Switches to the next favorite item
* `PUT /next/<iteration>`: Forwards the favorite items for the number of iterations. Limited to 10
* `GET /command?cmd=`: Wildcard, any command supported by the Denon receiver can be sent. Also works as 'PUT' or 'POST'

### Rest API Zone 2 Support
* `GET /z2/power`: Gets the current power level of zone 2
* `PUT /z2/power/<cmd>`: Sets the power of zone 2. Accepted values:
  - `on`, `standby` (same as `off`)
* `GET /z2/volume`: Gets the current volume level
* `PUT /z2/volume/<cmd>`: Sets the volume. Accepted values:
  - `up`, `down`, <volume> as decimal
* `GET /z2/mute`: Gets the zone 2 mute status
* `PUT /z2/mute/<cmd>`: Sets the zone 2 mute mode
  - `on`, `off`
* `GET /z2/source`: Gets the current zone 2 selected source
* `PUT /z2/source/<id>`: Sets the source zone 2. Accepted values:
  - `TUNER`, `DVD`, `BD`, `TV`, `SATCBL`, `MPLAY`, `GAME`, `AUX1`, `AUX2`, `NET`, `SPOTIFY`, `FLICKR`, `FAVORITES`, `IRADIO`, `SERVER`, `USBIPOD`
  - North America only: `PANDORA`, `SIRIUSXM`
  - Select and start playback: `USB` (USB), `IPD` (iPod),`IRP` (internet radio), `FVP` (favorites)

To control the connection to the receiver:
* `GET /connection`: Gets the TCP connection state of the 'Denon service' to the receiver.
* `PUT /connection/<command>`: Requests to change the connection state of 'Denon service' to the receiver. Accepted values: `connect`, `disconnect`

## Web Demo
For demonstration purpose there is a simple website which accesses the Denon service REST API.
Call `index.html` to use and test basic commands.
The recommendation is to build a separate website and only use the REST API of the Denon service.
